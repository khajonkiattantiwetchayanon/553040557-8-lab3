package tantiwetchayanon.khajonkiat.lab3;

public class GradeEvaluation {
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Usage:GradeEvaluation <score>");
			System.exit(1);
		}
		double score = Double.parseDouble(args[0]);
		computeGrade(score);
	}

	public static void computeGrade(double score) {
		char x;
		if (score > 100 || score < 0)
		{
			System.out.print(score + " is an invalid score. Please enter score in the range 0-100 ");
		}
		else 
		{
			if (score>=80)
			{
				x='A';
			}
			else if (score>=70)
			{
				x='B';
			}
			else if (score>=60)
			{
				x='C';
			}
			else if (score>=50)
			{
				x='D';
			}
			else
			{
				x='F';
			}
			System.out.print("The grade for score " + score + " is " + x );
		}
	}

}
