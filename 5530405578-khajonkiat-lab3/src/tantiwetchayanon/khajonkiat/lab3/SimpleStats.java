package tantiwetchayanon.khajonkiat.lab3;

import java.util.Arrays;

public class SimpleStats {
	
	
	public static void main (String[] args){
		double sum =0 ;
		int n = Integer.parseInt(args[0]);
		double num[] = new double[n];
		
		System.out.println("For the input GPAs: ");
		for (int i=0; i<n; i++)
		{
			num[i] = Double.parseDouble(args[i+1]);
			System.out.print( args[i+1] + " " );
			sum = sum + num[i];
		}

		Arrays.sort(num);
		System.out.println();
		System.out.println("star: ");
		System.out.println("Avg GPA is " + (sum/n) );
		System.out.println("Min GPA is " + num[0]);
		System.out.println("Max GPA is " + num[n-1]);
	}
	
	
	
	

}
